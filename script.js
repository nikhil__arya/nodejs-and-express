var method = module.exports = {};

var output = 100;

method.addSum = function (a,b) {
  output = a+b;
  return output;
}

method.multiply = function (a,b) {
  output = a*b;
  return output;
}

module.exports.data = method;
